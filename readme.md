**实现了一个Dotween超精简版的脚本，核心均基于Unity MonoNehaviour.StartCoroutine方法实现，添加了Unity.Transform添加扩展方法，方便调用。包含动画过程中常用的基本的位移、缩放、旋转、淡入淡出、以及数值的动态变化。使用也非常简单。直接将Plugins下的Prefabs拖动到场景中，然后再脚本中调用即可。**


## Demo截图预览<div align="center">
![](https://xyzip-1253271981.cos.ap-beijing-1.myqcloud.com/blog_image_bed/unity/AnimateLite/AnimateLite.gif)
---

## 部分代码如下

```csharp

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SunME {
	public static class Extensionmethod {
		/// <summary>
		/// 移动对象到目标位置
		/// </summary>
		/// <param name="targetPos">目标坐标</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoMove (this Transform t, Vector3 targetPos, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.MoveAtion (t, targetPos, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 旋转对象到目标欧拉角
		/// </summary>
		/// <param name="targetRotate">目标欧拉角</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoRotate (this Transform t, Vector3 targetRotate, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.RoateAction (t, targetRotate, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 缩放对象到目标值
		/// </summary>
		/// <param name="targetScale">目标缩放值</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale"></param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoScale (this Transform t, Vector3 targetScale, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.ScaleAction (t, targetScale, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 改变对象的Alpha为目标值
		/// </summary>
		/// <param name="isfadeIn">是否是淡入，否则为淡出</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoFade (this Transform t, bool isfadeIn, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.FadeAction (t, isfadeIn, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 在给定时间内改变一个浮点值
		/// </summary>
		/// <param name="targetValue">目标值</param>
		/// <param name="valueChangeAction">值发生改变时的回调函数</param>
		/// <param name="duration">持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">值更改完成时回调</param>
		public static void DoValueChange (this float t, float targetValue, Action<float> valueChangeAction, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.ValueChangeAction (t, targetValue, valueChangeAction, duration, ignoreTimeScale, OnComplete);
		}
	}
}
```
---

## 调用方式如下

```csharp
t1.DoMove (new Vector3 (3, -2, 0), 2, true, ()=>{ Debug.Log ("移动完成"); { Debug.Log ("移动完成"); });
		t2.DoRotate (new Vector3 (0, 60, 0), 2, false, () => { Debug.Log ("旋转完成"); });
		t3.DoScale (new Vector3 (3, 3, 3), 3);
		spriteRenderer.transform.DoFade (false, 3);
		image.transform.DoFade (false, 2);
		kk.DoValueChange (80, value => { _camera.fieldOfView = value; }, 2, false, () => {
				Debug.Log ("值改变完成");
			});
```

Demo和工程地质地址：扫码—>当前文章—>文章末尾<div align="center"> 						
![](https://img-blog.csdnimg.cn/20181108161954616.jpg)

## 地址：https://gitee.com/Sun_ME/AnimatedLite.git

         