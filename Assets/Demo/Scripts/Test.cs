﻿using System.Collections;
using System.Collections.Generic;
using SunME;
using UnityEngine;
using UnityEngine.UI;
public class Test : MonoBehaviour {

	public Transform t1;
	public Transform t2;
	public Transform t3;
	public Image image;
	public SpriteRenderer spriteRenderer;

	public Camera _camera;
	float kk = 0;

	void Update () {
		if (Input.GetKeyDown (KeyCode.A)) {
			Debug.Log (Time.deltaTime + "normal");
			//Debug.Log(Time.uns+"unscale");
			t1.DoMove (new Vector3 (3, -2, 0), 2, true, delegate () { Debug.Log ("移动完成"); });
		}
		if (Input.GetMouseButtonDown (1)) {
			Time.timeScale = 0;
		}
		if (Input.GetMouseButtonDown (2)) {
			Time.timeScale = 1;
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			t2.DoRotate (new Vector3 (0, 60, 0), 2, false, () => { Debug.Log ("旋转完成"); });
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			t3.DoScale (new Vector3 (3, 3, 3), 3);
		}
		if (Input.GetKeyDown (KeyCode.F)) {
			image.transform.DoFade (false, 2);
		}
		if (Input.GetKeyDown (KeyCode.G)) {
			spriteRenderer.transform.DoFade (false, 3);
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			Debug.Log ("按下了L");
			var starttime = Time.time;
			kk.DoValueChange (80, value => { _camera.fieldOfView = value; }, 2, false, () => {
				Debug.Log (Time.time - starttime + "花费时间为");
				Debug.Log ("值改变完成");
			});
		}
	}
}