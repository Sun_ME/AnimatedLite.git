﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SunME {
	public class Tween : MonoBehaviour {
		private static Tween _instance;
		public static Tween Instance {
			get { return _instance; }
		}
		void Awake () {
			_instance = this;
		}
		IEnumerator MoveIEnumator (Transform t, Vector3 targetPos, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			var time = 0.0f;
			var temp = t.position;
			while (time <= duration) {
				yield return null;
				time += ignoreTimeScale?Time.deltaTime : Time.deltaTime * Time.timeScale;
				t.localPosition = Vector3.Lerp (temp,
					targetPos, time / duration);
			}
			if (OnComplete != null) {
				OnComplete ();
			}
		}
		IEnumerator RotateIEnumator (Transform t, Vector3 targetRotate, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			var time = 0.0f;
			var temp = t.localEulerAngles;
			while (time <= duration) {
				yield return null;
				time += ignoreTimeScale?Time.deltaTime : Time.deltaTime * Time.timeScale;
				t.localEulerAngles = Vector3.Lerp (temp, targetRotate, time / duration);
			}
			if (OnComplete != null) {
				OnComplete ();
			}
		}
		IEnumerator ScaleIEnumator (Transform t, Vector3 targetScele, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			var time = 0.0f;
			var temp = t.localScale;
			while (time <= duration) {
				yield return null;
				time += ignoreTimeScale?Time.deltaTime : Time.deltaTime * Time.timeScale;
				t.localScale = Vector3.Lerp (temp, targetScele, time / duration);
			}
			if (OnComplete != null) {
				OnComplete ();
			}
		}
		IEnumerator FadeIEnumator (Transform t, bool isfadeIn, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			var color = new Color ();
			Image image = null;
			SpriteRenderer spriteRenderer = null;

			if (t.GetComponent<Image> ()) {
				color = t.GetComponent<Image> ().color;
				image = t.GetComponent<Image> ();
			} else if (t.GetComponent<SpriteRenderer> ()) {
				color = t.GetComponent<SpriteRenderer> ().color;
				spriteRenderer = t.GetComponent<SpriteRenderer> ();
			} else {
				Debug.LogError ("错误，缺少对应的可变色组件");
				yield break;
			}
			Debug.Log ("开始");
			var value = isfadeIn?new Color (color.r, color.g, color.b, color.a) : new Color (color.r, color.g, color.b, 1 - color.a);
			var time = 0.0f;
			var temp = color;
			var startTime = Time.time;
			while (time <= duration) {
				yield return null;
				time += ignoreTimeScale?Time.deltaTime : Time.deltaTime * Time.timeScale;
				color = Color.Lerp (temp, value, time / duration);
				if (image != null) image.color = color;
				if (spriteRenderer != null) spriteRenderer.color = color;
			}
			if (OnComplete != null) {
				OnComplete ();
			}
		}
		IEnumerator ValueChageIEnumator (float t, float targetValue,
			Action<float> valueChangeAction, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			var time = 0f;
			var temp = t;
			while (time <= duration) {
				yield return null;
				time += ignoreTimeScale?Time.deltaTime : Time.deltaTime * Time.timeScale;
				t = Mathf.Lerp (temp, targetValue, time / duration);
				if (valueChangeAction != null) {
					valueChangeAction (t);
				}
			}
			if (OnComplete != null) {
				OnComplete ();
			}
		}
		public void MoveAtion (Transform t, Vector3 targetPos, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			StartCoroutine (MoveIEnumator (t, targetPos, duration, ignoreTimeScale, OnComplete));
		}
		public void RoateAction (Transform t, Vector3 targetRotate, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			StartCoroutine (RotateIEnumator (t, targetRotate, duration, ignoreTimeScale, OnComplete));
		}
		public void ScaleAction (Transform t, Vector3 targetScale, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			StartCoroutine (ScaleIEnumator (t, targetScale, duration, ignoreTimeScale, OnComplete));
		}
		public void FadeAction (Transform t, bool isfadeIn, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			StartCoroutine (FadeIEnumator (t, isfadeIn, duration, ignoreTimeScale, OnComplete));
		}
		public void ValueChangeAction (float t, float targetValue, Action<float> valueChangeAction, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			StartCoroutine (ValueChageIEnumator (t, targetValue, valueChangeAction, duration, ignoreTimeScale, OnComplete));
		}
	}
}