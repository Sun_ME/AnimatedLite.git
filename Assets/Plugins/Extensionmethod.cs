﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SunME {
	public static class Extensionmethod {
		/// <summary>
		/// 移动对象到目标位置
		/// </summary>
		/// <param name="targetPos">目标坐标</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoMove (this Transform t, Vector3 targetPos, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.MoveAtion (t, targetPos, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 旋转对象到目标欧拉角
		/// </summary>
		/// <param name="targetRotate">目标欧拉角</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoRotate (this Transform t, Vector3 targetRotate, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.RoateAction (t, targetRotate, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 缩放对象到目标值
		/// </summary>
		/// <param name="targetScale">目标缩放值</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale"></param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoScale (this Transform t, Vector3 targetScale, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.ScaleAction (t, targetScale, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 改变对象的Alpha为目标值
		/// </summary>
		/// <param name="isfadeIn">是否是淡入，否则为淡出</param>
		/// <param name="duration">过程的持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">执行完成的回调函数</param>
		public static void DoFade (this Transform t, bool isfadeIn, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.FadeAction (t, isfadeIn, duration, ignoreTimeScale, OnComplete);
		}
		/// <summary>
		/// 在给定时间内改变一个浮点值
		/// </summary>
		/// <param name="targetValue">目标值</param>
		/// <param name="valueChangeAction">值发生改变时的回调函数</param>
		/// <param name="duration">持续时间</param>
		/// <param name="ignoreTimeScale">是否忽视时间缩放</param>
		/// <param name="OnComplete">值更改完成时回调</param>
		public static void DoValueChange (this float t, float targetValue, Action<float> valueChangeAction, float duration = 1, bool ignoreTimeScale = false, Action OnComplete = null) {
			Tween.Instance.ValueChangeAction (t, targetValue, valueChangeAction, duration, ignoreTimeScale, OnComplete);
		}
	}
}